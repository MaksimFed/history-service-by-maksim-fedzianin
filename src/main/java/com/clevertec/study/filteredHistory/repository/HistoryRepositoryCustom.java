package com.clevertec.study.filteredHistory.repository;

import com.clevertec.study.filteredHistory.dto.FilterForHistory;
import com.clevertec.study.filteredHistory.dto.History;
import org.springframework.data.domain.Page;

public interface HistoryRepositoryCustom {
    Page<History> getHistory(FilterForHistory filterForHistory, Integer pageSize, Integer pageNumber);
}
