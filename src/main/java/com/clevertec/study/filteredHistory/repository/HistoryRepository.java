package com.clevertec.study.filteredHistory.repository;

import com.clevertec.study.filteredHistory.dto.History;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface HistoryRepository extends MongoRepository<History, String>, HistoryRepositoryCustom{

}
