package com.clevertec.study.filteredHistory.util;

public class Constants {
    public static final String HISTORY_REQUEST_MAPPING = "/history";
    public static final String HISTORY_POST_LOG = "POST, body : {}";
    public static final String FILTER_POST_MAPPING = "/filter";
    public static final String PAGENUMBER_PARAMETER_NAME = "pagenumber";
    public static final String PAGESIZE_PARAMETER_NAME = "pagesize";
    public static final String PAGENUMBER_DEFAULT_VALUE = "0";
    public static final String PAGESIZE_DEFAULT_VALUE = "2";
    public static final String GET_HISTORY_BY_ID_MAPPING = "/{id}";
    public static final String HISTORY_GET_BY_ID_LOG = "GET by id, requested id : {}";

    public static final String ERROR_DURING_JSON_PARSING = "Error during JSON parsing of object {}," +
            "exception : {}";
    public static final String HISTORY_SAVED = "History saved : {}";
    public static final String HISTORY_UPDATED = "History updated : old value: {}, new value : {}";
    public static final String HISTORY_RECEIVED = "History received : requested id: {}, user : {}";
    public static final String HISTORY_DELETED = "History with id = {} deleted, old value : {}";
    public static final String HISTORY_UPDATE_NOT_FOUND = "History with id = {} not exist";
    public static final String HISTORY_DELETE_LOG = "DELETE by id called, id = {}";
    public static final String HISTORY_PUT_LOG = "PUT called, body : {}";
    public static final String DELETE_BY_ID_MAPPING = "/{id}";
    public static final String UPDATE_BY_ID_MAPPING = "/{id}";

}
