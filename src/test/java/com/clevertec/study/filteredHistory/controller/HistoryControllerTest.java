package com.clevertec.study.filteredHistory.controller;

import com.clevertec.study.filteredHistory.dto.History;
import com.clevertec.study.filteredHistory.service.HistoryService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Date;
import java.util.Optional;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@RunWith(SpringRunner.class)
@WebMvcTest(HistoryController.class)
public class HistoryControllerTest {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    HistoryService historyService;

    private static History history;

    @BeforeClass
    public static void init() {
        history = new History();
        history.setUserName("userName1");
        history.setTimestamp(new Date());
        history.setOperatingType("registration");
        history.setEntityType("type1");
        history.setIsWaslStatus(true);
        history.setIsWialonStatus(false);
        history.setWaslDescription("desc1");
        history.setWialonDescription("desc11");
        history.setEntityDescription("entity description");
    }

    @Test
    public void createCorrectHistory() throws Exception {
        String expectedId = "123zxc";

        given(this.historyService.save(history)).willReturn(expectedId);

        this.mockMvc.perform(MockMvcRequestBuilders
                .post("/history")
                .content(asJsonString(history))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string(expectedId));
    }

    @Test
    public void getExistingHistoryReturnExistingHistory() throws Exception {

        String expectedId = "2";

        history.setId(expectedId);
        given(historyService.findById(expectedId)).willReturn(Optional.of(history));


        this.mockMvc.perform(MockMvcRequestBuilders.get("/history/{id}", expectedId)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(jsonPath("$.id").exists())
                .andExpect(jsonPath("$.userName").exists())
                .andExpect(jsonPath("$.timestamp").exists())
                .andExpect(jsonPath("$.operatingType").exists())
                .andExpect(jsonPath("$.entityType").exists())
                .andExpect(jsonPath("$.isWaslStatus").exists())
                .andExpect(jsonPath("$.isWialonStatus").exists())
                .andExpect(jsonPath("$.waslDescription").exists())
                .andExpect(jsonPath("$.wialonDescription").exists())
                .andExpect(jsonPath("$.entityDescription").exists())
                .andExpect(jsonPath("$.id").value(expectedId))
                .andExpect(jsonPath("$.userName").value(history.getUserName()))
                //.andExpect(jsonPath("$.timestamp").value(asJsonString(history.getTimestamp())))
                .andExpect(jsonPath("$.operatingType").value(history.getOperatingType()))
                .andExpect(jsonPath("$.entityType").value(history.getEntityType()))
                .andExpect(jsonPath("$.isWaslStatus").value(history.getIsWaslStatus()))
                .andExpect(jsonPath("$.isWialonStatus").value(history.getIsWialonStatus()))
                .andExpect(jsonPath("$.waslDescription").value(history.getWaslDescription()))
                .andExpect(jsonPath("$.wialonDescription").value(history.getWialonDescription()))
                .andExpect(jsonPath("$.entityDescription").value(history.getEntityDescription()))
        ;

    }

    @Test
    public void getNotExistingHistoryReturnNotFound() throws Exception {

        given(historyService.findById(anyString())).willReturn(Optional.empty());

        this.mockMvc.perform(MockMvcRequestBuilders.get("/user/{id}", "abc123")
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().isNotFound());
    }


    public static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}