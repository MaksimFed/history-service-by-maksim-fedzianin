package com.clevertec.study.filteredHistory.service;

import com.clevertec.study.filteredHistory.dto.FilterForHistory;
import com.clevertec.study.filteredHistory.dto.History;
import org.springframework.data.domain.Page;

import java.util.Optional;

public interface HistoryService {

    public String save(History history);

    public Page<History> getHistory(FilterForHistory filterForHistory, Integer pageSize, Integer pageNum);

    Optional<History> findById(String id);

    Optional<History> deleteById(String id);

    Optional<History> update(History history);
}
