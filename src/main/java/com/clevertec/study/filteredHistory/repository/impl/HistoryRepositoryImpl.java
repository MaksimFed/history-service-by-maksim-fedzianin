package com.clevertec.study.filteredHistory.repository.impl;

import com.clevertec.study.filteredHistory.dto.FilterForHistory;
import com.clevertec.study.filteredHistory.dto.History;
import com.clevertec.study.filteredHistory.repository.HistoryRepositoryCustom;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@RequiredArgsConstructor
public class HistoryRepositoryImpl implements HistoryRepositoryCustom {

    private final MongoTemplate mongoTemplate;

    @Override
    public Page<History> getHistory(FilterForHistory filterForHistory,
                                    Integer pageSize, Integer pageNumber) {
        log.info("get Filtered history called");
        Pageable pageable = PageRequest.of(pageNumber, pageSize);
        Query query = new Query();
        query.with(pageable);
        addFilterConstraints(query, filterForHistory);

        List<History> histories = mongoTemplate.find(query, History.class);
        Page<History> result = new PageImpl<History>(histories, pageable, mongoTemplate.count(query, History.class));
        return result;
    }

    private void addFilterConstraints(Query query, FilterForHistory filterForHistory) {

        Map<String, Object> filterFields = getFilterFields(filterForHistory);

        List<Criteria> criterias = new ArrayList<>();
        for (String fieldName : filterFields.keySet()){
            Object fieldValue = filterFields.get(fieldName);
            if (fieldValue != null){
                if (fieldName.startsWith("is")){
                    String parameter = fieldName.substring("is".length());
                    log.info("parameter : {}", parameter);
                    criterias.add(Criteria.where(parameter).is(fieldValue));
                }
                if (fieldName.endsWith("From")){
                    String parameter = fieldName.substring(0,fieldName.length() - "From".length());
                    log.info("parameter : {}", parameter);
                    criterias.add(Criteria.where(parameter).gte(fieldValue));
                }
                if (fieldName.endsWith("To")){
                    String parameter = fieldName.substring(0,fieldName.length() - "To".length());
                    log.info("parameter : {}", parameter);
                    criterias.add(Criteria.where(parameter).lt(fieldValue));
                }
                if (fieldName.endsWith("s")){
                    String parameter = fieldName.substring(0, fieldName.length() - "s".length());
                    log.info("parameter : {}", parameter);
                    criterias.add(Criteria.where(parameter).in((Object[]) fieldValue));
                }
            }
        }
        Criteria criteria = new Criteria().andOperator(criterias.toArray(new Criteria[criterias.size()]));
        query.addCriteria(criteria);
        log.info("formed query :" + query);
    }

    private Map<String, Object> getFilterFields(FilterForHistory filterForHistory){
        Map<String, Object> filterFields = new HashMap<>();
        Class<? extends FilterForHistory> filterClass = filterForHistory.getClass();
        Field[] fields = filterClass.getDeclaredFields();
        for (Field field : fields) {
            try {
                field.setAccessible(true);
                filterFields.put(field.getName(), field.get(filterForHistory));
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        return  filterFields;
    }
}
