package com.clevertec.study.filteredHistory.controller;

import com.clevertec.study.filteredHistory.dto.FilterForHistory;
import com.clevertec.study.filteredHistory.dto.History;
import com.clevertec.study.filteredHistory.service.HistoryService;
import com.clevertec.study.filteredHistory.util.Constants;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping(Constants.HISTORY_REQUEST_MAPPING)
@Slf4j
@RequiredArgsConstructor
public class HistoryController {

    private final HistoryService historyService;

    @PostMapping(consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> saveHistory(@RequestBody History history) {
        log.info(Constants.HISTORY_POST_LOG, history);
        try {
            return ResponseEntity.ok(historyService.save(history));
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE,
            value = Constants.FILTER_POST_MAPPING)
    public ResponseEntity<?> getHistory(@RequestBody FilterForHistory filterForHistory,
                                        @RequestParam(name = Constants.PAGESIZE_PARAMETER_NAME,
                                                defaultValue = Constants.PAGESIZE_DEFAULT_VALUE,
                                                required = false)
                                                Integer pageSize,
                                        @RequestParam(name = Constants.PAGENUMBER_PARAMETER_NAME,
                                                defaultValue = Constants.PAGENUMBER_DEFAULT_VALUE,
                                                required = false)
                                                Integer pageNumber) {
        log.info(Constants.HISTORY_POST_LOG, filterForHistory);
        try {
            return ResponseEntity.ok(historyService.getHistory(filterForHistory, pageSize, pageNumber));
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping(value = Constants.GET_HISTORY_BY_ID_MAPPING,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> getHistoryById(@PathVariable String id) {
        log.info(Constants.HISTORY_GET_BY_ID_LOG, id);
        Optional<History> history = historyService.findById(id);
        if (history.isPresent()) {
            return ResponseEntity.ok(history);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping(value = Constants.DELETE_BY_ID_MAPPING,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> deleteHistory(@PathVariable String id) {
        log.info(Constants.HISTORY_DELETE_LOG, id);
        Optional<History> history = historyService.deleteById(id);
        if (history.isPresent()) {
            return ResponseEntity.ok(history);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping(value = Constants.UPDATE_BY_ID_MAPPING,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> updateUser(@RequestBody History history, @PathVariable String id) {
        log.info(Constants.HISTORY_PUT_LOG, history);
        history.setId(id);
        Optional<History> h = historyService.update(history);
        if (h.isPresent()) {
            return ResponseEntity.ok(history);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
