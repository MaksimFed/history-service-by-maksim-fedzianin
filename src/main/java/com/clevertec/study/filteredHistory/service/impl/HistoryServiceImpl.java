package com.clevertec.study.filteredHistory.service.impl;

import com.clevertec.study.filteredHistory.dto.FilterForHistory;
import com.clevertec.study.filteredHistory.dto.History;
import com.clevertec.study.filteredHistory.repository.HistoryRepository;
import com.clevertec.study.filteredHistory.service.HistoryService;
import com.clevertec.study.filteredHistory.util.Constants;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@Slf4j
@RequiredArgsConstructor
public class HistoryServiceImpl implements HistoryService {

    private final HistoryRepository historyRepository;
    private final ObjectMapper objectMapper;

    @Override
    public String save(History history) {
        historyRepository.save(history);
        log.info(Constants.HISTORY_SAVED, history);
        return history.getId();
    }

    @Override
    public Page<History> getHistory(FilterForHistory filterForHistory, Integer pageSize, Integer pageNum) {
        return historyRepository.getHistory(filterForHistory, pageSize, pageNum);
    }

    @Override
    public Optional<History> findById(String id) {
        Optional<History> history = historyRepository.findById(id);
        log.info(Constants.HISTORY_RECEIVED, id, history);
        return history;
    }

    @Override
    public Optional<History> deleteById(String id) {
        Optional<History> history = historyRepository.findById(id);
        historyRepository.deleteById(id);
        log.info(Constants.HISTORY_DELETED, id, history);
        return history;
    }

    @Override
    public Optional<History> update(History history) {
        Optional<History> h = historyRepository.findById(history.getId());
        if (!h.isPresent()) {
            log.info(Constants.HISTORY_UPDATE_NOT_FOUND, history.getId());
            return h;
        }
        Optional<History> oldHistory = h;
        historyRepository.save(history);
        log.info(Constants.HISTORY_UPDATED, h, history);
        return Optional.of(history);
    }

    private String toJson(Object o) {
        String json = null;
        try {
            json = objectMapper.writeValueAsString(o);
        } catch (JsonProcessingException e) {
            log.warn(Constants.ERROR_DURING_JSON_PARSING, o.getClass().getSimpleName(), e.toString());
        }
        return json;
    }
}
