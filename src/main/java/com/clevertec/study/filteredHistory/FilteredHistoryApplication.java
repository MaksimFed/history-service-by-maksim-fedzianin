package com.clevertec.study.filteredHistory;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FilteredHistoryApplication {

	public static void main(String[] args) {
		SpringApplication.run(FilteredHistoryApplication.class, args);
	}

}
